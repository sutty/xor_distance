package xor_distance

import (
  "errors"
)

func Distance(a string, b string) ([]rune, error) {
  c := []rune(a)
  d := []rune(b)

  if len(c) != len(d) {
    return nil, errors.New("strings must be same length")
  }

  r := []rune{}

  for i, e := range a {
    r = append(r, e ^ d[i])
  }

  return r, nil
}

func Compare(a []rune, b []rune) (int, error) {
  if len(a) != len(b) {
    return 0, errors.New("strings must be same length")
  }

  for i, c := range a {
    if c == b[i] {
      continue
    } else if c < b[i] {
      return -1, nil
    } else {
      return 1, nil
    }
  }

  return 0, nil
}

func GreaterThan(a []rune, b []rune) (bool, error) {
  r, err := Compare(a, b)

  if err != nil {
    return false, err
  }

  return (r == -1), err
}

func LessThan(a []rune, b []rune) (bool, error) {
  r, err := Compare(a, b)

  if err != nil {
    return false, err
  }

  return (r == 1), err
}

func Equal(a []rune, b []rune) (bool, error) {
  r, err := Compare(a, b)

  if err != nil {
    return false, err
  }

  return (r == 0), err
}
