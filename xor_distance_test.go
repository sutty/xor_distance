package xor_distance

import (
  "testing"
  "reflect"
)

func TestDistance(t *testing.T) {
  dist, err := Distance("antifa", "fachos")
  want := []rune{7, 15, 23, 1, 9, 18}

  if !reflect.DeepEqual(want, dist) || err != nil {
    t.Fatalf(`Distance("antifa", "fachos") must be %v`, want)
  }
}

func TestUnicodeDistance(t *testing.T) {
  dist, err := Distance("solidaridad", "filantropía")
  want := []rune{21, 6, 0, 8, 10, 21, 0, 6, 20, 140, 5}

  if !reflect.DeepEqual(want, dist) || err != nil {
    t.Fatalf(`Distance("solidaridad", "filantropía") must be %v`, want)
  }
}

func TestCompare(t *testing.T) {
  dist1, _ := Distance("antifa", "fachos")
  dist2, _ := Distance("fachos", "antifa")
  comp, err := Compare(dist1, dist2)
  want := 0

  if want != comp || err != nil {
    t.Fatalf(`Compare("antifa", "fachos") must be %v`, want)
  }
}

func TestGreaterThan(t *testing.T) {
  dist1, _ := Distance("antifa", "fachos")
  dist2, _ := Distance("fachos", "antifa")
  gt, err := GreaterThan(dist1, dist2)
  want := false

  if want != gt || err != nil {
    t.Fatalf(`GreaterThan("antifa", "fachos") must be %v`, want)
  }
}

func TestLessThan(t *testing.T) {
  dist1, _ := Distance("antifa", "fachos")
  dist2, _ := Distance("fachos", "antifa")
  lt, err := LessThan(dist1, dist2)
  want := false

  if want != lt || err != nil {
    t.Fatalf(`LessThan("antifa", "fachos") must be %v`, want)
  }
}

func TestEqualNot(t *testing.T) {
  dist1, _ := Distance("antifa", "fachos")
  dist2, _ := Distance("tachos", "antifx")
  eq, err := Equal(dist1, dist2)
  want := false

  if want != eq || err != nil {
    t.Fatalf(`Equal("antifa", "fachos") must be %v`, want)
  }
}

func TestEqual(t *testing.T) {
  dist1, _ := Distance("antifa", "fachos")
  dist2, _ := Distance("fachos", "antifa")
  eq, err := Equal(dist1, dist2)
  want := true

  if want != eq || err != nil {
    t.Fatalf(`Equal("antifa", "antifa") must be %v`, want)
  }
}
