Calculate XOR distance between strings.

```go
import "0xacab.org/sutty/xor_distance"

distance = xor_distance.Distance("antifa", "fachos")
```
